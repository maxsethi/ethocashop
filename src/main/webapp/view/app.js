var app = angular.module('EthocaStore', ['ui.grid','ngRoute','ui.grid.pagination']);


app.config(function($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl : "default.html"
        })
        .when("/cart", {
            templateUrl : "cart.html"
        })
        .when("/checkout", {
            templateUrl : "confirmation.html"
        });
});


app.controller('ProductCtrl', ['$scope','ProductService', 'CartService', function ($scope, ProductService, CartService) {

    var paginationOptions = {
     pageNumber: 1,
	 pageSize: 8,
	 sortName: 'id',
     sortDirection: 'desc'
   };

    $scope.cart=CartService.cart;


    ProductService.getProducts(paginationOptions.pageNumber,
		   paginationOptions.pageSize,paginationOptions.sortName,paginationOptions.sortDirection).success(function(data){
	  $scope.gridOptions.data = data.content;
	  $scope.pageResult=data;
 	  $scope.gridOptions.totalItems = data.totalElements;
      $scope.pageSize=paginationOptions.pageSize;
      $scope.pageNumber=paginationOptions.pageNumber;
      $scope.totalItems=data.totalElements;
      $scope.sortName=paginationOptions.sortName;
      $scope.sortDirection=paginationOptions.sortDirection;
   });
   
   $scope.gridOptions = {
    paginationPageSizes: [8, 17, 25],
    rowHeight: 120,
    paginationPageSize: paginationOptions.pageSize,
    enableColumnMenus:false,
	useExternalPagination: true,
    columnDefs: [
      { headerCellClass: 'text-center', width:'100', minWidth:'70', enableSorting: false, cellClass:'text-center', name: '', field:'imageUrl', cellTemplate:"<img width=\"70px\" ng-src=\"{{grid.getCellValue(row, col)}}\" lazy-src>"},
      { name: 'name' , cellTemplate:'productInfo.html', minWidth:'200'},
      { name: 'price', width:'150', field:'price', cellClass:'text-center',cellFilter: 'currency'},
      { name: 'stock', width:'200', cellTemplate: 'productQuantity.html', enableSorting: false},
      { name:'Action', cellTemplate:'productAction.html', width:'200', enableSorting: false}
    ],
    onRegisterApi: function(gridApi) {
        $scope.gridApi = gridApi;
        gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
          paginationOptions.pageNumber = newPage;
          paginationOptions.pageSize = pageSize;
          ProductService.getProducts(newPage,pageSize,$scope.sortName,$scope.sortDirection).success(function(data){
        	  $scope.gridOptions.data = data.content;
              $scope.pageResult=data;
              $scope.pageSize=pageSize;
              $scope.pageNumber=paginationOptions.pageNumber;
              $scope.totalItems=data.totalElements;
         	  $scope.gridOptions.totalItems = data.totalElements;
          });
        });

        gridApi.core.on.sortChanged($scope, function(grid, sortColumns){
            if(typeof sortColumns[0] == 'undefined') {
                $scope.sortName = 'id';
                $scope.sortDirection = 'desc';
            }
            else{
                $scope.sortName=sortColumns[0].name;
                $scope.sortDirection=sortColumns[0].sort.direction;
            }

            ProductService.getProducts($scope.pageNumber,$scope.pageSize,$scope.sortName,$scope.sortDirection).success(function(data){
                $scope.gridOptions.data = data.content;
                $scope.pageResult=data;
                $scope.totalItems=data.totalElements;
                $scope.gridOptions.totalItems = data.totalElements;
            });

        });

     }
  };
}]);

app.service('ProductService',['$http', function ($http) {
	
	function getProducts(pageNumber,size,sortName,sortDirection) {
		pageNumber = pageNumber > 0?pageNumber - 1:0;
        return  $http({
          method: 'GET',
          url: 'product/get?page='+pageNumber+'&size='+size+'&sortName='+sortName+'&sortDirection='+sortDirection
        });
    }
	
    return {
    	getProducts:getProducts
    };
}]);


// CartService to share cart between difference views!
app.factory("CartService", function () {

    var angularCart = new shoppingCart("AngularStore");

    return { cart: angularCart };
});