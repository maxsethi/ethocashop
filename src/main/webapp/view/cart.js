/**
 * Created by karansethi on 2018-03-08.
 */

function shoppingCart(cartName) {
    this.cartName = cartName;
    this.clearCart = false;
    this.checkoutParameters = {};
    this.products = [];

    // load products from local storage when initializing
    this.loadItems();

    // save products to local storage when unloading
    var self = this;
    $(window).unload(function () {
        if (self.clearCart) {
            self.clearItems();
        }
        self.saveItems();
        self.clearCart = false;
    });
}

function cartItem(sku, name, price, quantity) {
    this.sku = sku;
    this.name = name;
    this.price = price * 1;
    this.quantity = quantity * 1;
}


// Load shopping cart from Local Storage
shoppingCart.prototype.loadItems = function () {
    var products = localStorage != null ? localStorage[this.cartName + "_products"] : null;
    if (products != null && JSON != null) {
        try {
            var products = JSON.parse(products);
            for (var i = 0; i < products.length; i++) {
                var product = products[i];
                if (product.sku != null && product.name != null && product.price != null && product.quantity != null) {
                    product = new cartItem(product.sku, product.name, product.price, product.quantity);
                    this.products.push(product);
                }
            }
        }
        catch (err) {
            console.log("Exception occurred while loading shopping cart "+err);
        }
    }
}

// save products to local storage
shoppingCart.prototype.saveItems = function () {
    if (localStorage != null && JSON != null) {
        localStorage[this.cartName + "_products"] = JSON.stringify(this.products);
    }
}

// Add Product to LocalStorage
shoppingCart.prototype.addProduct = function (sku, name, price, quantity) {
    quantity = this.toNumber(quantity);
    if (quantity != 0) {

        // update quantity for existing product
        var found = false;
        for (var i = 0; i < this.products.length && !found; i++) {
            var product = this.products[i];
            if (product.sku == sku) {
                found = true;
                product.quantity = this.toNumber(product.quantity + quantity);
                if (product.quantity <= 0) {
                    this.products.splice(i, 1);
                }
            }
        }

        // new product, add now
        if (!found) {
            var product = new cartItem(sku, name, price, quantity);
            this.products.push(product);
        }

        // save changes
        this.saveItems();
    }
}

// Updates Quantity in the cart!
shoppingCart.prototype.updateQuantity = function (sku, quantity) {
    quantity = this.toNumber(quantity);


    // Update quantity for existing Product
    for (var i = 0; i < this.products.length; i++) {
        var product = this.products[i];
        if (product.sku == sku) {
            found = true;
            if (quantity <= 0) {
                this.products.splice(i, 1);
            }
            else {
                product.quantity = quantity;
            }
            break;
        }
    }
    // save changes
    this.saveItems();
}

// An Item Total Price from local Storage on based of sku
shoppingCart.prototype.getTotalPrice = function (sku) {
    var total = 0;
    for (var i = 0; i < this.products.length; i++) {
        var product = this.products[i];
        if (sku == null || product.sku == sku) {
            total += this.toNumber(product.quantity * product.price);
        }
    }
    return total;
}

// An Item Total Count from local Storage on based of sku
shoppingCart.prototype.getTotalCount = function (sku) {
    var count = 0;
    for (var i = 0; i < this.products.length; i++) {
        var product = this.products[i];
        if (sku == null || product.sku == sku) {
            count += this.toNumber(product.quantity);
        }
    }
    return count;
}

// clear the cart
shoppingCart.prototype.clearItems = function () {
    this.products = [];
    this.saveItems();
}

shoppingCart.prototype.checkout = function () {
    this.clearItems();
    window.location.href='#/checkout'
}

shoppingCart.prototype.toNumber = function (value) {
    value = value * 1;
    return isNaN(value) ? 0 : value;
}
