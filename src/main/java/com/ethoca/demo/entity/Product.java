package com.ethoca.demo.entity;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by karansethi on 2018-03-03.
 */
@Entity
public @Data
class Product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Min(value = 0, message = "*SKU has to be non negative number")
    @Column(name = "sku", unique = true, nullable = true)
    private String sku;

    @Basic
    @Column(name="date_created", nullable = false)
    private Date dateCreated;

    @Basic
    @Column(name="last_updated", nullable = false)
    private Date lastUpdated;

    @Length(min = 5, message = "*Name must have at least 5 characters")
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description", nullable = true)
    private String description;

    @Column(name = "imageUrl", nullable = true)
    private String imageUrl;

    @Min(value = 0, message = "*Quantity has to be non negative number")
    @Column(name = "quantity", nullable = false)
    private Integer quantity;

    @DecimalMin(value = "0.00", message = "*Price has to be non negative number")
    @Column(name = "price", nullable = true)
    private Double price;

    public Product(String name, String description, Integer quantity, Double price, String imageUrl, String sku) {
        this.name = name;
        this.description = description;
        this.quantity = quantity;
        this.price = price;
        this.imageUrl=imageUrl;
        this.sku=sku;
    }
    public Product(){}

    @PrePersist
    protected void onCreate(){
        setDateCreated(new Date());
        setLastUpdated(new Date());
    }

    @PreUpdate
    protected void onUpdate(){
        setLastUpdated(new Date());
    }

}
