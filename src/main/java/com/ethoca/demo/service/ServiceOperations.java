package com.ethoca.demo.service;

import org.springframework.data.domain.Page;

public interface ServiceOperations<T> {

    public Page<T> findPaginated(final Integer page, final Integer size, String sortName, String sortDirection);

}
