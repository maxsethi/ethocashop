package com.ethoca.demo.service;

import com.ethoca.demo.entity.Product;

import java.util.List;

/**
 * Created by karansethi on 2018-03-03.
 */
public interface ProductService extends ServiceOperations<Product> {

    public List<Product> resetProducts();

}
