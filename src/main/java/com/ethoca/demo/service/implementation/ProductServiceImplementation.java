package com.ethoca.demo.service.implementation;

import com.ethoca.demo.entity.Product;
import com.ethoca.demo.repository.ProductRepository;
import com.ethoca.demo.service.ProductService;
import com.mysql.jdbc.StringUtils;
import org.fluttercode.datafactory.impl.DataFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karansethi on 2018-03-06.
 */
@Service
public class ProductServiceImplementation implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Value("${demo.imagePath}")
    private String imagePath;

    public List<Product> getFakeProducts(){
        List<Product> products = new ArrayList<>();
        DataFactory df = new DataFactory();
        for(long i=5; i<30; i++) {
            StringBuilder description = new StringBuilder();
            for(int j=1; j<i; j++) {
                description.append(df.getRandomWord(8, false)+"  ");
            }
            products.add(new Product(df.getRandomText(5,10), description.toString(), df.getNumberBetween(0, 20), (double)df.getNumberBetween(800, 2000), imagePath, df.getNumberText(10)));
        }
        return products;
    }

    public List<Product> resetProducts(){
        productRepository.deleteAll();
        List<Product> products = getFakeProducts();
        productRepository.saveAll(products);
        return products;
    }

    @Override
    public Page<Product> findPaginated(Integer page, Integer size, String sortName, String sortDirection) {
        Sort.Direction sortDirectionType = Sort.Direction.DESC;
        if(page==null || size==null)
        {
            page = 0;
            size = 10;
        }
        try {
            if (StringUtils.isEmptyOrWhitespaceOnly(sortName) || StringUtils.isEmptyOrWhitespaceOnly(sortDirection)) {
                sortName = "id";
                sortDirection = "desc";
            }
            if (sortDirection.equalsIgnoreCase("asc")) {
                sortDirectionType = Sort.Direction.ASC;
            }
            return productRepository.findAll(PageRequest.of(page, size, new Sort(sortDirectionType, sortName)));
        }
        catch(Exception e)
        {
            return Page.empty();
        }
    }

}
