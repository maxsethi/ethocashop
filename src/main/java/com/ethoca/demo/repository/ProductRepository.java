package com.ethoca.demo.repository;

import com.ethoca.demo.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by karansethi on 2018-03-03.
 */
public interface ProductRepository extends JpaRepository<Product, Long> {
}
