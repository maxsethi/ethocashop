package com.ethoca.demo.controller.rest;

import com.ethoca.demo.entity.Product;
import com.ethoca.demo.handler.CustomContentNotFoundException;
import com.ethoca.demo.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by karansethi on 2018-03-03.
 */
@RestController
@RequestMapping("/product")
@Log
@Api(value="products", description="Operations pertaining to products in Ethoca Online Store!")
public class ProductController {

    @Autowired
    ProductService productService;

    /**
     * Reset Products Table with 25 new sample Products
     * @return List of Products
     */
    @ApiOperation(value = "Reset Products Table with 25 new dynamically generated Products", response = List.class)
    @RequestMapping(value = "/reset", method = RequestMethod.GET)
    public List<Product> resetProducts(){
        return productService.resetProducts();
    }

    /**
     * Return List Of Products based on pagination parameters, page number and page size.
     * @param page Specify the page number
     * @param size Specify the page size
     * @return Pages of Products including pagination attributed like totalElemets etc.
     */

    @ApiOperation(value = "Return List Of Products based on pagination parameters.", response = Page.class)
    @RequestMapping(value = "/get", method = RequestMethod.GET, produces = "application/json")
    public Page<Product> findPaginated(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size,
                                       @RequestParam(value = "sortName", required = false) String sortName,
                                       @RequestParam(value = "sortDirection", required = false) String sortDirection){
        Page<Product> resultPage = productService.findPaginated(page, size, sortName, sortDirection);
        if (page!=null && page > resultPage.getTotalPages()) {
            throw new CustomContentNotFoundException();
        }
        return resultPage;
    }
}
