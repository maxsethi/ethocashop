package com.ethoca.demo;

import com.ethoca.demo.service.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by karansethi on 2018-03-11.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ShoppingValleyApplication.class)
@WebAppConfiguration
public class ProductControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Autowired
    private ProductService productService;

    private static boolean initialized = false;

    @Before
    public void setup()
    {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        if (!initialized) {
            productService.resetProducts();
            initialized = true;
        }
    }

    @Test
    public void validate_InvalidRequest_ResponseCode() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/reset"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void resetEndPoint_Response_JsonArrayTest()
            throws Exception {

        mockMvc.perform(get("/product/reset").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(25)));
    }

    @Test
    public void getEndpoint_Missing_Parameters_Test()
            throws Exception {

        mockMvc.perform(get("/product/get").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void getEndpoint_Invalid_PageNum_Test()
            throws Exception {

        mockMvc.perform(get("/product/get?page=-1&size=1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("content", hasSize(0)));
    }

    @Test
    public void getEndpoint_Invalid_Size_Test()
            throws Exception {

        mockMvc.perform(get("/product/get?page=0&size=0").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("content", hasSize(0)));
    }

    @Test
    public void getEndpoint_Validate_Sample_Response()
            throws Exception {

        mockMvc.perform(get("/product/get?page=0&size=8").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("content", hasSize(8)));
    }

    @Test
    public void getEndpoint_Validate_Pagination()
            throws Exception {

        mockMvc.perform(get("/product/get?page=1&size=8").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("content", hasSize(8)));
    }

}
